# -*- coding: UTF-8 -*-
'''
Created on 3 févr. 2014
updated on 2014-10-05 -- Python 3.4
@author: Johnny Tsheke
'''

liste=[10.0, 40.2, 30E-2, "a", 150E0, 7.3]
somme=0
for nombre in liste:
    if not isinstance(nombre,float):
        print("Arrêt de la boucle: ",nombre," n'est pas un nombre réel\n")
        break
    somme = somme+ nombre
else:
    print("La somme de tous les nombres réel est: ",somme)
print("La somme calculée est: ",somme)
print("Fin du programme\n")