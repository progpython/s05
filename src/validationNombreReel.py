# -*- coding: UTF-8 -*-
'''
Created on 2014-02-02
updated on 2014-10-05 -- Python 3.4
@author: Johnny Tsheke
'''
bonNombre=False
while not bonNombre:
    nombre=input("Entrer un nombre réel svp\n")
    #nombre=eval(nombre)
    if(isinstance(nombre,float)):
        bonNombre=True
        print("nombre réel",nombre," accepté\n")
    else:
        print(nombre," n'est pas un nombre réel\n")