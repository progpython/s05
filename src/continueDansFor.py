# -*- coding: UTF-8 -*-
'''
Created on 3 febr. 2014
updated on 2022-05-14 -- Python 3.10
@author: jtsheke
'''

liste=[10.0, 40.2, 30E-2, "a", 150E0, 7.3]
somme=0
for nombre in liste:
    if not isinstance(nombre,float):
        print("Erreur ",nombre," n'est pas un nombre réel. On n'additionne pas ..\n")
        continue
    somme = somme+ nombre

print("La somme calculée est: ",somme)
print("Fin du programme\n")