# -*- coding: UTF-8 -*-
'''
Created on 2014-02-02
updated on 2022-05-15 -- Python 3.10
@author: Johnny Tsheke
'''

reponsesValides=['O','o','N','n']
reponse=input("Repondez par O ou o (pour oui)/N ou n (pour non) svp\n")
while reponse not in reponsesValides:
    reponse=input("Repondez par O ou o (pour oui)/N ou n (pour non) svp\n")

print("Votre réponse est: ",reponse)