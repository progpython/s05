# -*- coding: UTF-8 -*-
'''
Created on 2014-10-05
updated on 2022-05-15 -- Python 3.10
@author: Johnny Tsheke
'''    
#exemple Pour vérifier si un dictionnaire contient une clé ou une vleur donnée

dic={1:"Marte",3:"sae",5:"40"}
cles=dic.keys() # les clés du dictionnaire

print(list(cles)) # list pour convertir en liste
cle=6 # une clé quelconque.
if(cle in dic.keys()): # on verifie si le dictionnaite contient la clé
  print(cle," est une clé du dictionnaire")
else:
  print(cle," n'est pas une clé du dictionnaire")

valeurs=dic.values() # valeurs du dictionnaire
print(list(valeurs)) # list pour convertir en liste 
 
val="sae" # une valeur quelqconque
if(val in dic.values()):
  print("Ce dictionnaire contient la valeur ",val)
else:
  print("Ce dictionnaaire ne contieent pas de valeur ",val)


