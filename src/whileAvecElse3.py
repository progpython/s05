# -*- coding: UTF-8 -*-
'''
Created on 2014-02-01
updated on 2014-10-05 -- Python 3.4
@author: Johnny Tsheke
'''

MAX = 10
MILIEU = 5
nb = 1
while nb <= MAX:
    if(nb == MILIEU):
        break #la partie else ne s'exécutera pas
    print(nb)
    nb=nb+1
else:
    print("tous les éléments sont affichés")

print("fin du programme")