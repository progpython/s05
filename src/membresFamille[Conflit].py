# -*- coding: UTF-8 -*-
'''
Created on 31 janv. 2014
@author: Johnny Tsheke
'''
from __future__ import print_function 
print("Gestion des informations des membres d'une famille")
famille=[]
oui=['o','O']
continuer='N' #Non
cond1,numero=True,1
while ((continuer in oui) or cond1):
        cond1=False
        print("Entrez les information du membre numero ",numero,"  de la famille svp\n")
        prenom=raw_input("Entrer le prenom de la personne \n")
        pers={"Prenom":prenom,"Numero":numero}
        famille.append(pers)
        del(pers) #on efface la variable pour eviter toute confusion
        numero=numero+1
        continuer=raw_input("Continuer? Repondez par O,o,n, ou N sans guillemet")
#Maintenant on va afficher la liste de la famille
nbPersonnes=len(famille) # nombre d'elément dans la liste
index=0;
print("Voici les membres de la famille enregistrés\n")
while (index<nbPersonnes):
        pers=famille[index]
        print("Numero, Prénom:", end=" ") 
        print(pers["Numero"],pers["Prenom"],sep=", ",end=".\n")
        index=index+1      
print("\n Merci d'avoir utiliser ce programme\n")  